//
//  RockPaperScissorsApp.swift
//  RockPaperScissors
//
//  Created by Pascal Hintze on 23.10.2023.
//

import SwiftUI

@main
struct RockPaperScissorsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
