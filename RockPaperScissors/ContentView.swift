//
//  ContentView.swift
//  RockPaperScissors
//
//  Created by Pascal Hintze on 23.10.2023.
//

import SwiftUI

struct ContentView: View {
    let moves = ["Rock", "Paper", "Scissor"]
    let winningMoves = ["Paper", "Scissor", "Rock"]
    
    @State private var appsChoice = Int.random(in: 0...2)
    
    @State private var playerShouldWin = Bool.random()
    @State private var playersScore = 0
    @State private var playersChoice = ""
    @State private var playedRounds = 0
    @State private var reachedMaxRounds = false
    
    var body: some View {
        VStack {
            Spacer()
            
            Text("Score: \(playersScore)")
            Text(moves[appsChoice])
            if playerShouldWin == true {
                Text("You should win.")
            } else {
                Text("You should loose.")
            }
            
            Spacer()
            
            HStack {
                Button("Rock", action: {whoWon(moveNumber: 2)
                  print("Winning move is = \(winningMoves[2])")
                })
                Button("Paper", action: {whoWon(moveNumber: 0)
                    print("Winning move is = \(winningMoves[0])")
                })
                Button("Scissor", action: {whoWon(moveNumber: 1)
                    print("Winning move is = \(winningMoves[1])")
                })
            }
            
            Spacer()
        }
        .padding()
        .alert("Score", isPresented: $reachedMaxRounds) {
            Button("Continue", action: resetGame)
        } message: {
           Text("\(playersScore)")
        }
    }
    
    
    func whoWon(moveNumber: Int) {
        
        if playerShouldWin == true {
            if appsChoice == moveNumber {
                playersScore += 1
            } else {
                playersScore -= 1
            }
            
        }
        if playerShouldWin == false {
            if appsChoice != moveNumber {
                playersScore += 1
            } else {
                playersScore -= 1
            }
        }
        
        
        playedRounds += 1
        appsChoice = Int.random(in: 0...2)
        playerShouldWin.toggle()
        
        if playedRounds == 10 {
            reachedMaxRounds = true
        }
        
    }
    
    func resetGame() {
        playersScore = 0
        appsChoice = Int.random(in: 0...2)
        playerShouldWin.toggle()
    }
}

#Preview {
    ContentView()
}
